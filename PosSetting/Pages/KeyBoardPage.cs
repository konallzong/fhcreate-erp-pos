﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace PosSetting.Pages
{
    public partial class KeyBoardPage : UserControl,BasePage
    {
        FileKeyBoardService service = FileKeyBoardService.getService();
        public KeyBoardPage()
        {
            InitializeComponent();
            this.dataGridView1.AutoGenerateColumns = false;
            loadFromDb();
        }

        void loadFromDb()
        {
            List<KeyBoard> keys = service.listAll();
            this.dataGridView1.DataSource = keys;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter && dataGridView1.Focused)
            {
                KeyBoard kb = dataGridView1.CurrentRow.DataBoundItem as KeyBoard;
                KeyForm kf = new KeyForm(kb.keyCode, kb.commandKey, kb.commandName);
                kf.ShowDialog();
                loadFromDb();
                return true;
            }
            else
            {
                return false;
            }
        }


        public void save()
        {
            
        }

        public void test()
        {
            
        }
    }
}
