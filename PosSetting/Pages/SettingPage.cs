﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Printer;

namespace PosSetting.Pages
{
    public partial class SettingPage : UserControl,BasePage
    {
        public SettingPage()
        {
            InitializeComponent();
            String po = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintOption);
            pNone.Checked = "0".Equals(po);
            pCom.Checked = "1".Equals(po);
            pLtp.Checked = "2".Equals(po);
            pUsb.Checked = "3".Equals(po);
            pFile.Checked = "4".Equals(po);
            this.comPort.Text = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComPort);
            this.comRate.Text = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComRate);
            this.comBit.Text = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComBit);
            this.comStopBit.Text = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComStopBit);
            this.comCheckBit.Text = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintComCheckBit);
            this.lptProt.Text = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.LptProt);
            this.printFileName.Text = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintFile);
        }

        public void save()
        {
            String po = "";
            po = pNone.Checked ? "0" : po;
            po = pCom.Checked ? "1" : po;
            po = pLtp.Checked ? "2" : po;
            po = pUsb.Checked ? "3" : po;
            po = pFile.Checked ? "4" : po;
            InIUtil.WriteInIData(InIUtil.PrintSection,InIUtil.PrintOption, po);
            InIUtil.WriteInIData(InIUtil.PrintSection, InIUtil.PrintComPort, this.comPort.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.PrintSection, InIUtil.PrintComRate, this.comRate.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.PrintSection, InIUtil.PrintComBit, this.comBit.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.PrintSection, InIUtil.PrintComStopBit, this.comStopBit.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.PrintSection, InIUtil.PrintComCheckBit, this.comCheckBit.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.PrintSection, InIUtil.LptProt, this.lptProt.SelectedItem.ToString());
            InIUtil.WriteInIData(InIUtil.PrintSection, InIUtil.PrintFile, this.printFileName.Text);
            MessageBox.Show("保存成功");
        }

        public void test()
        {
            String po = InIUtil.ReadInIData(InIUtil.PrintSection,InIUtil.PrintOption);
            PrintBase pb = null;
            if ("1".Equals(po))
            {
                pb = new ComPrinter();
            }
            else if ("2".Equals(po))
            {
                pb = new LPTPrinter();
            }
            else if ("3".Equals(po))
            {
                pb = new USBPrinter();
            }
            else if ("4".Equals(po))
            {
                pb = new FilePrinter();
            }
            pb.initPrinter();
            pb.open();
            pb.writeLine("打印机测试成功");
            pb.writeLine("abcdefghijklmnopqrstuvwxyz");
            pb.writeLine("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
            pb.writeLine("1234567890.!@#$%^&*()_+=-");
            pb.NewRow();
            pb.NewRow();
            pb.NewRow();
            pb.Dispose();
        }
    }
}
