﻿using PosSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.Printer
{
    public class PrinterManager
    {
        private static PrintBase pb;
        public static PrintBase getPrinter()
        {
            String printOption = InIUtil.ReadInIData(InIUtil.PrintSection, InIUtil.PrintOption);
            if(pb == null)
            {
                //初始化打印

                if ("1".Equals(printOption))
                {
                    pb = new ComPrinter();
                }
                else if ("2".Equals(printOption))
                {
                    pb = new LPTPrinter();
                }
                else if ("3".Equals(printOption))
                {
                    pb = new USBPrinter();
                }
                else if ("4".Equals(printOption))
                {
                    pb = new FilePrinter();
                }
            }
            return pb;
        }

    }
}
