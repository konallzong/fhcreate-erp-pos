﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Pay
{
    interface IMalomePayLogger
    {
        Boolean isDebugEnable();

        void debug(String log);
    }
}
