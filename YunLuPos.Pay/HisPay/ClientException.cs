﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HisPay.Payment
{
    class ClientException: ApplicationException
    {
        public static ClientException LOG_LOAD_ERROR = new ClientException("LOG_LOAD_ERROR", "日志配置错误");
        public static ClientException READ_CONFIG = new ClientException("READ_CONFIG_ERROR", "读取配置文件错误");
        public static ClientException ENCRYPT_ERROR = new ClientException("ENCRYPT", "数据加密错误");
        public static ClientException DECRYPT_ERROR = new ClientException("DECRYPT", "数据解密错误");
        public static ClientException ENCODE_ERROR = new ClientException("ENCODE_ERROR", "序列化数据错误");
        public static ClientException DECODE_ERROR = new ClientException("DECODE_ERROR", "解析数据错误");

        public static ClientException NET_URL_ERROR = new ClientException("NET_URL_ERROR", "服务器路径配置错误");
        public static ClientException NET_ERROR = new ClientException("NET_ERROR", "网络连接异常");
        public static ClientException NET_TIMEOUT_ERROR = new ClientException("NET_TIMEOUT_ERROR", "获取数据超时,支付结果未知");
        public static ClientException NET_UNKNOW_ERROR = new ClientException("NET_UNKNOW_ERROR", "未知网络错误,支付结果未知");

        public static ClientException SERVER_ERROR = new ClientException("SERVER_ERROR", "服务器返回错误,支付结果未知");
        public static ClientException SERVER_NO_BODY = new ClientException("SERVER_NO_BODY", "服务器未返回结果,支付结果未知");
        public static ClientException SERVICE_NAME_ERROR = new ClientException("SERVICE_NAME_ERROR", "未找到服务路径");
        private String code;
        private String msg;
        public ClientException(String code, String message)
        {
            this.code = code;
            this.msg = message;
        }
        public String getCode()
        {
            return code;
        }

        public String getMsg()
        {
            return msg;
        }

        public Dictionary<String, Object> toMap()
        {
            Dictionary<String, Object> result = new Dictionary<String, Object>();
            result.Add(C.RESULT_CODE, C.FAIL);
            result.Add(C.FAIL_CODE, getCode());
            result.Add(C.RESULT_MSG, getMsg());
            return result;
        }
    }
}
