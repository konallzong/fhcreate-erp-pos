﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HisPay.Payment
{
    public class C
    {
        public static String SERVER_PAY = "pay";
	    public static String SERVER_QUERY = "query";
	    public static String SERVER_REFUND = "refund";
	    public static String SERVER_REFUND_QUERY = "refundQuery";
	    public static String SERVER_CANCEL = "cancel";
	
	
	    public static String RESULT_CODE = "RESULT_CODE";
	    public static String FAIL_CODE = "FAIL_CODE";
	    public static String RESULT_MSG = "RESULT_MSG";
	
	    public static String SUCCESS = "SUCCESS";
	    public static String FAIL = "FAIL";

    }
}
