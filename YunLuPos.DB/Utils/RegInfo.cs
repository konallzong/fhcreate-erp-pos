﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.DB.Utils
{
    public class RegInfo
    {
        public String serverAddr { get; set; }

        public String regCode { get; set; }

        public String deviceNum { get; set; }

        public String clientKey { get; set; }

        public String cliqueCode { get; set; }

        public String cliqueName { get; set; }

        public String branchCode { get; set; }

        public String branchName { get; set; }

        public String posCode { get; set; }

        public String posName { get; set; }

        public String regTime { get; set; }

        public String expTime { get; set; }
    }
}
