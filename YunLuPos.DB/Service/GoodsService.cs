﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.DB.Service
{
    public class GoodsService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(GoodsService));

        /**
         * 根据条码或商品编码获取商品
         * */
        public Goods getGoodsByCode(String code)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Goods goods = db.Queryable<Goods>().Where("status != 'DELETE' and( barCode = @barCode or goodsCode = @goodsCode)", new { barCode = code , goodsCode = code }).SingleOrDefault();
                    return goods;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }

        /**
        * 新增或更新商品
        * */
        public Int64 insertOrUpdate(Goods goods)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Goods dbGoods = db.Queryable<Goods>()
                        .Where(it => it.barCode == goods.barCode)
                        .SingleOrDefault();
                    if (dbGoods != null)
                    {
                        db.Delete<Goods>(it => it.barCode == dbGoods.barCode);
                        return (Int64)db.Insert<Goods>(goods);
                    }
                    else
                    {
                        return (Int64)db.Insert<Goods>(goods);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return 0;
        }

        /**
        * 批量新增或更新商品
        * */
        public Int64 insertOrUpdate(List<Goods> goods)
        {
            if(goods == null || goods.Count <= 0)
            {
                return 0;
            }
            DBLocker.setLocker();
            SqlSugarClient db = null;
            try
            {
                using (db = SugarDao.GetInstance())
                {
                    db.BeginTran();
                    foreach(Goods g in goods)
                    {
                        if ("DELETE".Equals(g.status) || g.deleteBarCode != null)
                        {
                            //删除条码
                            db.Delete<Goods>(it => it.goodsCode == g.goodsCode && it.barCode == g.deleteBarCode);
                        }else
                        {
                            Goods dbGoods = db.Queryable<Goods>()
                                .Where(it => it.goodsCode == g.goodsCode &&  it.barCode == g.barCode)
                                .SingleOrDefault();
                            if (dbGoods != null)
                            {
                                db.Delete<Goods>(it => it.goodsCode == g.goodsCode && it.barCode == dbGoods.barCode);
                            }
                            db.Insert<Goods>(g);
                        }

                    }
                    db.CommitTran();
                }
            }
            catch (Exception ex)
            {
                db.RollbackTran();
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return goods.Count;
        }


        public String maxVersion()
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Object obj = db.Queryable<Goods>().Max(it => it.ver);
                    if(obj == null || "".Equals(obj.ToString()))
                    {
                        return "0";
                    }else
                    {
                        return obj.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return "0";
        }

        /**
         * 根据条码或商品编码获取商品
         * */
        public List<Goods> listTouchGoods()
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    return db.Queryable<Goods>().Where("status != 'DELETE'").ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }
    }
}
