﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.DB;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;

namespace YunLuPos.Net.Http
{
    public class MebHttp : HttpBase, MebNetService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(MebHttp));

        public Member pay(MebBizBody biz)
        {
            try
            {
                MebBizDTO dto = new MebBizDTO();
                dto.bizType = "2";//消费
                String bizJson = JsonConvert.SerializeObject(biz);
                bizJson = DESUtils.encrypt(bizJson);
                dto.body = bizJson;
                return doRequest<Member>(dto, "pos/member/biz");
            }
            catch (NetException ne)
            {
                logger.Error(ne);
                throw ne;
            }
        }

        public Member query(MebBizBody biz)
        {
            try
            {
                MebBizDTO dto = new MebBizDTO();
                dto.bizType = "1";//查询
                String bizJson = JsonConvert.SerializeObject(biz);
                bizJson = DESUtils.encrypt(bizJson);
                dto.body = bizJson;
                return doRequest<Member>(dto, "pos/member/biz");
            }
            catch (NetException ne)
            {
                logger.Error(ne);
                throw ne;
            }
        }

        public Member search(MebSearchInfo info)
        {
            try
            {
                return doRequest<Member>(info, "pos/member/search");
            }
            catch (NetException ne)
            {
                logger.Error(ne);
                return null;
            }
            catch (BizError be)
            {
                logger.Error(be);
                return null;
            }
            
        }

      
    }
}
