﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Net.Http
{
    public class NetException: ApplicationException
    {
        public static NetException NET_URL_ERROR = new NetException("NET_URL_ERROR", "服务器路径配置错误");
        public static NetException NET_ERROR = new NetException("NET_ERROR", "网络连接异常");
        public static NetException NET_TIMEOUT_ERROR = new NetException("NET_TIMEOUT_ERROR", "获取数据超时");
        public static NetException NET_UNKNOW_ERROR = new NetException("NET_UNKNOW_ERROR", "未知网络错误");

        public static NetException SERVER_ERROR = new NetException("SERVER_ERROR", "服务器内部错误");
        public static NetException SERVER_NO_BODY = new NetException("SERVER_NO_BODY", "服务器未返回结果");
        public static NetException SERVICE_NAME_ERROR = new NetException("SERVICE_NAME_ERROR", "未找到服务路径");

        
        private String code;
        private String msg;
        public NetException(String code, String message)
        {
            this.code = code;
            this.msg = message;
        }
        public String getCode()
        {
            return code;
        }

        public String getMsg()
        {
            return msg;
        }

        public Dictionary<String, Object> toMap()
        {
            Dictionary<String, Object> result = new Dictionary<String, Object>();
            result.Add("RESULT_CODE", "FAIL");
            result.Add("FAIL_CODE", getCode());
            result.Add("RESULT_MSG", getMsg());
            return result;
        }
    }
}
