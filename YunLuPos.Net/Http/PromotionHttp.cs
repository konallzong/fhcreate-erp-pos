﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;

namespace YunLuPos.Net.Http
{
    public class PromotionHttp : HttpBase, PromotionNetService
    {
        public List<Promotion> list(PromotionVer maxVersion)
        {
            return doRequest<List<Promotion>>(maxVersion, "pos/promotion/list");
        }
    }
}
