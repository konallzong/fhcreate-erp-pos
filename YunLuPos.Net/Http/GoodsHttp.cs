﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Net.DTO;

namespace YunLuPos.Net.Http
{
    public class GoodsHttp:HttpBase,GoodsNetService
    {
        SaleOrderService orderService = new SaleOrderService();
        GoodsService goodsService = new GoodsService();

        public List<long> ids(GoodsInfoVer maxVersion)
        {
            return doRequest<List<long>>(maxVersion, "pos/goods/ids");
        }

        public List<Goods> list(List<long> ids)
        {
            return doRequest<List<Goods>>(ids, "pos/goods/list");
        }

    }
}
