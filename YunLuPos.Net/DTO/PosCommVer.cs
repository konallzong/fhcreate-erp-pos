﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Net.DTO
{
    public class PosCommVer
    {
        public String cliqueCode { get; set; }

        public String branchCode { get; set; }

        public String posCode { get; set; }

        public String payTypeVer { get; set; }

        public String cashierVer { get; set; }
    }
}
