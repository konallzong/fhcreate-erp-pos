﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class PosInfo
    {
        public String cliqueCode{get;set;}

        public String cliqueName { get; set; }

        public String branchCode{get;set;}

        public String branchName { get; set; }

        public String posCode{get;set;}

        public String posName{get;set;}

        public String ipAddress{get;set;}

        public String regCode{get;set;}

        public String deviceCode{get;set;}

        public String createDate { get; set; }

        public String expDate { get; set; }

        public String appKey { get; set; }

    }
}
