﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;

namespace YunLuPos.Com
{
    public partial class GoodsInfoPanel : UserControl
    {
        public delegate void CodeSearchHandle(String value, GoodsInfoPanel handel);

        public event CodeSearchHandle CodeSearchEvent;

        public GoodsInfoPanel()
        {
            InitializeComponent();
        }

        public void setFocus()
        {
            this.codeInput.Focus();
            Console.WriteLine("33333333333333333333");
        }

        public void setGoods(Goods goods)
        {
            this.codeInput.Text = "";
            if (goods == null)
            {
                this.goodsName.Text = "无";
                this.specs.Text = "无";
                this.unit.Text = "无";
                this.price.Text = "0.00";
            }
            else
            {
                this.goodsName.Text = goods.goodsName;
                this.specs.Text = goods.specs;
                this.unit.Text = goods.unitName;
                this.price.Text = String.Format("{0:C}", goods.salePrice);

            }
        }

        private void baseTextInput1_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            const char Enter = (char)13;
            if (e.KeyChar == Enter)
            {
                CodeSearchEvent?.Invoke(codeInput.Text, this);
                e.Handled = true;
            }
            else if (!(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != Delete)
            {
                e.Handled = true;
            }
        }

        private void codeInput_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
