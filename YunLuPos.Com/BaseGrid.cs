﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YunLuPos.Com
{
    public partial class BaseGrid : DataGridView
    {
        public Color CustomMainColor { get; set; }
        public Color CustomFoceColor { get; set; }
        public BaseGrid()
        {
            InitializeComponent();
        }

        public BaseGrid(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        private void rowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(e.RowBounds.Location.X,
            e.RowBounds.Location.Y,
            RowHeadersWidth - 4,
            e.RowBounds.Height);
            if (CurrentRow != null)
            {
                TextRenderer.DrawText(e.Graphics, (e.RowIndex + 1).ToString(),
                      RowHeadersDefaultCellStyle.Font,
                      rectangle,
                      (CurrentRow != null && (e.RowIndex == CurrentRow.Index)) ? CustomMainColor : CustomFoceColor,
                      TextFormatFlags.VerticalCenter | TextFormatFlags.Right);

            }
        }

        private void paint(object sender, PaintEventArgs e)
        {
            this.BackgroundColor = CustomMainColor;
            this.GridColor = CustomFoceColor;

            ColumnHeadersDefaultCellStyle.BackColor = CustomMainColor;
            ColumnHeadersDefaultCellStyle.ForeColor = CustomFoceColor;

            RowHeadersDefaultCellStyle.BackColor = CustomMainColor;
            RowHeadersDefaultCellStyle.ForeColor = CustomFoceColor;
            RowHeadersDefaultCellStyle.SelectionBackColor = CustomFoceColor;
            RowHeadersDefaultCellStyle.SelectionForeColor = CustomMainColor;


            DefaultCellStyle.BackColor = CustomMainColor;
            DefaultCellStyle.ForeColor = CustomFoceColor;
            DefaultCellStyle.SelectionBackColor = CustomFoceColor;
            DefaultCellStyle.SelectionForeColor = CustomMainColor;

        }

        /**
         * 下移选中行
         * 到最后一行停止
         * */
        public void moveUp()
        {
            if (RowCount == 0)
            {
                return;
            }
            if (CurrentRow.Index > 0)
            {
                int moveRow = CurrentRow.Index - 1;
                CurrentCell = Rows[moveRow].Cells[0];
            }
        }

        /**
        * 上一选中行
        * 到第一行停止
        * */
        public void moveDown()
        {
            if (RowCount == 0)
            {
                return;
            }
            if (CurrentRow.Index < RowCount - 1)
            {
                int moveRow = CurrentRow.Index + 1;
                CurrentCell = Rows[moveRow].Cells[0];
            }
        }
    }
}
