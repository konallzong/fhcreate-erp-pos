﻿namespace YunLuPos.Com
{
    partial class TopInfoPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.orderType = new YunLuPos.Com.BaseLabel(this.components);
            this.cashierName = new YunLuPos.Com.BaseLabel(this.components);
            this.posCode = new YunLuPos.Com.BaseLabel(this.components);
            this.date = new YunLuPos.Com.BaseLabel(this.components);
            this.shopName = new YunLuPos.Com.BaseLabel(this.components);
            this.orderNo = new YunLuPos.Com.BaseLabel(this.components);
            this.time = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel4 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel3 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel2 = new YunLuPos.Com.BaseLabel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackgroundImage = global::YunLuPos.Com.Properties.Resources.net_alive;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(717, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(15, 15);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // orderType
            // 
            this.orderType.AutoSize = true;
            this.orderType.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.orderType.ForeColor = System.Drawing.Color.White;
            this.orderType.Location = new System.Drawing.Point(3, 8);
            this.orderType.Name = "orderType";
            this.orderType.Size = new System.Drawing.Size(31, 12);
            this.orderType.TabIndex = 11;
            this.orderType.Text = "销售";
            // 
            // cashierName
            // 
            this.cashierName.AutoSize = true;
            this.cashierName.ForeColor = System.Drawing.Color.White;
            this.cashierName.Location = new System.Drawing.Point(484, 8);
            this.cashierName.Name = "cashierName";
            this.cashierName.Size = new System.Drawing.Size(11, 12);
            this.cashierName.TabIndex = 10;
            this.cashierName.Text = "*";
            // 
            // posCode
            // 
            this.posCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.posCode.AutoSize = true;
            this.posCode.ForeColor = System.Drawing.Color.White;
            this.posCode.Location = new System.Drawing.Point(617, 8);
            this.posCode.Name = "posCode";
            this.posCode.Size = new System.Drawing.Size(11, 12);
            this.posCode.TabIndex = 9;
            this.posCode.Text = "*";
            // 
            // date
            // 
            this.date.AutoSize = true;
            this.date.ForeColor = System.Drawing.Color.White;
            this.date.Location = new System.Drawing.Point(383, 8);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(65, 12);
            this.date.TabIndex = 8;
            this.date.Text = "0000-00-00";
            // 
            // shopName
            // 
            this.shopName.AutoSize = true;
            this.shopName.ForeColor = System.Drawing.Color.White;
            this.shopName.Location = new System.Drawing.Point(169, 8);
            this.shopName.Name = "shopName";
            this.shopName.Size = new System.Drawing.Size(11, 12);
            this.shopName.TabIndex = 7;
            this.shopName.Text = "*";
            // 
            // orderNo
            // 
            this.orderNo.AutoSize = true;
            this.orderNo.ForeColor = System.Drawing.Color.White;
            this.orderNo.Location = new System.Drawing.Point(33, 8);
            this.orderNo.Name = "orderNo";
            this.orderNo.Size = new System.Drawing.Size(11, 12);
            this.orderNo.TabIndex = 6;
            this.orderNo.Text = "0";
            // 
            // time
            // 
            this.time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.time.AutoSize = true;
            this.time.ForeColor = System.Drawing.Color.White;
            this.time.Location = new System.Drawing.Point(659, 8);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(53, 12);
            this.time.TabIndex = 4;
            this.time.Text = "00:00:00";
            // 
            // baseLabel4
            // 
            this.baseLabel4.AutoSize = true;
            this.baseLabel4.ForeColor = System.Drawing.Color.White;
            this.baseLabel4.Location = new System.Drawing.Point(451, 8);
            this.baseLabel4.Name = "baseLabel4";
            this.baseLabel4.Size = new System.Drawing.Size(35, 12);
            this.baseLabel4.TabIndex = 3;
            this.baseLabel4.Text = "人员:";
            // 
            // baseLabel3
            // 
            this.baseLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.baseLabel3.AutoSize = true;
            this.baseLabel3.ForeColor = System.Drawing.Color.White;
            this.baseLabel3.Location = new System.Drawing.Point(585, 8);
            this.baseLabel3.Name = "baseLabel3";
            this.baseLabel3.Size = new System.Drawing.Size(35, 12);
            this.baseLabel3.TabIndex = 2;
            this.baseLabel3.Text = "终端:";
            // 
            // baseLabel2
            // 
            this.baseLabel2.AutoSize = true;
            this.baseLabel2.ForeColor = System.Drawing.Color.White;
            this.baseLabel2.Location = new System.Drawing.Point(350, 8);
            this.baseLabel2.Name = "baseLabel2";
            this.baseLabel2.Size = new System.Drawing.Size(35, 12);
            this.baseLabel2.TabIndex = 1;
            this.baseLabel2.Text = "日期:";
            // 
            // TopInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.orderType);
            this.Controls.Add(this.cashierName);
            this.Controls.Add(this.posCode);
            this.Controls.Add(this.date);
            this.Controls.Add(this.shopName);
            this.Controls.Add(this.orderNo);
            this.Controls.Add(this.time);
            this.Controls.Add(this.baseLabel4);
            this.Controls.Add(this.baseLabel3);
            this.Controls.Add(this.baseLabel2);
            this.Name = "TopInfoPanel";
            this.Size = new System.Drawing.Size(740, 28);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private BaseLabel baseLabel3;
        private BaseLabel time;
        private BaseLabel orderNo;
        private BaseLabel shopName;
        private BaseLabel date;
        private BaseLabel posCode;
        private BaseLabel cashierName;
        private BaseLabel baseLabel2;
        private BaseLabel baseLabel4;
        private BaseLabel orderType;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
