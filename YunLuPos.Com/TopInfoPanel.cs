﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;
using YunLuPos.Entity.Constant;

namespace YunLuPos.Com
{
    public partial class TopInfoPanel : UserControl
    {
        public TopInfoPanel()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            date.Text = dt.ToString("yyyy-MM-dd");
            time.Text = dt.ToString("HH:mm:ss");
        }

        /**
         * 设置静态显示信息
         * 门店号 机器号
         * 收银员
         * */
        public void setCommInfo(CommInfo comminfo)
        {
            if(comminfo == null)
            {
                this.shopName.Text = "";
                this.cashierName.Text = "";
                this.posCode.Text = "";
            }else
            {
                this.shopName.Text = comminfo.shopName;
                this.cashierName.Text = comminfo.cashierName;
                this.posCode.Text = comminfo.posCode;
            }
        }

        /**
         * 设置订单编号与订单类型
         * */
        public void setOrderInfo(SaleOrder order)
        {
            this.orderNo.Text = order.orderCode;
            if (SaleOrderType.SALE.ToString().Equals(order.orderType))
            {
                this.orderType.ForeColor = Colors.foreColor;
                this.orderType.Text = "销售";

            }else if (SaleOrderType.REJECT.ToString().Equals(order.orderType))
            {
                this.orderType.ForeColor = Colors.errorColor;
                this.orderType.Text = "退货";
            }
        }


        /**
         * 刷新网络状况
         * */
        public void setNetStats(Boolean isAlive)
        {
            if (isAlive)
            {
                this.pictureBox1.BackgroundImage = global::YunLuPos.Com.Properties.Resources.net_alive;
            }else
            {
                this.pictureBox1.BackgroundImage = global::YunLuPos.Com.Properties.Resources.net_down;
            }
        }
    }
}
