﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Payment
{
    public class ResultMessage
    {
        public static String ERROR = "ERROR";
        public static String SUCCESS = "SUCCESS";

        private String code;

        private String errorMessage;

        public static  ResultMessage createError(String errorMessage)
        {
            ResultMessage rm = new ResultMessage();
            rm.code = ERROR;
            rm.errorMessage = errorMessage;
            return rm;
        }
        public static ResultMessage createSuccess()
        {
            ResultMessage rm = new ResultMessage();
            rm.code = SUCCESS;
            return rm;
        }

        public string Code
        {
            get
            {
                return code;
            }

            set
            {
                code = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }

            set
            {
                errorMessage = value;
            }
        }
    }
}
