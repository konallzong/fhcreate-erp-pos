﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Payment
{
    class CardPayment : PaymentInterface
    {
        private CodesService codesService = new CodesService();
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        public ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            //扫码支付
            String outTradeNo = codesService.genPayOutTradeNo(order.orderCode);
            SellCardPayForm sacnForm = new SellCardPayForm(outTradeNo, inputAmount);
            DialogResult result = sacnForm.ShowDialog();
            if (result == DialogResult.OK)
            {
                //返回错误码
                String errorMessage = accountService.addAccount(order, payType, state, inputAmount);
                if (errorMessage != null)
                {
                    return ResultMessage.createError(errorMessage);
                }
                else
                {
                    return ResultMessage.createSuccess();
                }
            }
            else
            {
                return ResultMessage.createError("储值卡消费取消");
            }
        }

        public string getKey()
        {
            return "CARDPAY";
        }

        public List<string> print()
        {
            return new List<string>();
        }
    }
}
