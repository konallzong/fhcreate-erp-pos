﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace YunLuPos.Payment
{
    public class CashPayment : PaymentInterface
    {
        private SaleOrderAccountService accountService = new SaleOrderAccountService();

        public string getKey()
        {
            return "CASH";
        }

        public ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            //返回错误码
            String errorMessage = accountService.addAccount(order, payType, state, inputAmount);
            if(errorMessage != null)
            {
                return ResultMessage.createError(errorMessage);
            }else
            {
                return ResultMessage.createSuccess();
            }
        }

        public List<string> print()
        {
            List<String> list = new List<string>();
            return list;
        }
    }
}
