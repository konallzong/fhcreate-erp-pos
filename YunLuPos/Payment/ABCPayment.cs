﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.Payment
{
    public class ABCPayment : PaymentInterface
    {

        [DllImport(@"\abcmis\ChaseInterface.dll", EntryPoint = "Abmcs", SetLastError = true,
         CharSet = CharSet.Auto, ExactSpelling = true,
         CallingConvention = CallingConvention.StdCall)]
            public static extern int Abmcs(IntPtr strIn, IntPtr strOut);

        public ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            String strIn = payInfoToString( order,  payType,  state,  inputAmount);
            String strOut = call(strIn);
            Console.WriteLine(strOut);
            ABCPayResult pr = stringToPayResult(strOut);
            if(pr == null)
            {
                return ResultMessage.createError("农行接口未返回结果");
            }
            if ("00".Equals(pr.Result))
            {
               return ResultMessage.createSuccess();
            }else
            {
                return ResultMessage.createError(pr.ResultDescribe);
            }
        }

        private String call(String strIn)
        {
            IntPtr ptrIn = Marshal.StringToHGlobalAnsi(strIn);
            IntPtr ptrOut = Marshal.AllocHGlobal(1024);
            Abmcs(ptrIn, ptrOut);
            String strOut = Marshal.PtrToStringAnsi(ptrOut);
            return strOut;
        }

        public string getKey()
        {
            return "ABCMISPOS";
        }

        /// <summary>
        /// 农行接口支付字符串转换
        /// </summary>
        /// <param name="strOut"></param>
        /// <returns></returns>
        private ABCPayResult stringToPayResult(String strOut)
        {
            String[] arr = strOut.Split('|');
            ABCPayResult pr = new ABCPayResult();
            pr.SourceString = strOut;
            pr.Result = arr[0];
            pr.AuthNum = arr[1];
            pr.CardNum = arr[2];
            pr.Amount = arr[3];
            pr.SystemNum = arr[4];
            pr.Validity = arr[5];
            pr.PayDate = arr[6];
            pr.PayTime = arr[7];
            pr.ResultDescribe = arr[8];
            pr.TenantNum = arr[9];
            pr.DriveNum = arr[10];
            pr.CardType = arr[11];
            //pr.BankNum = arr[12];
            // pr.BankName = arr[13];
            return pr;
        }

        //农业银行接口输入字符串转换
        private String payInfoToString(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            if (order == null || inputAmount <= 0)
            {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("0"); //0消费 4退货
            sb.Append("|");
            String amountStr = (inputAmount * 100).ToString();
            amountStr = amountStr.PadLeft(12, '0');
            sb.Append(amountStr);
            sb.Append("|");
            sb.Append(StaticInfoHoder.commInfo.cashierCode);
            sb.Append("|");
            sb.Append(order.orderCode);
            sb.Append("|");
            //sb.Append(payinfo.SourceSystemNum);
            sb.Append("");
            sb.Append("|");
            sb.Append(StaticInfoHoder.commInfo.posCode);
            sb.Append("|");
            return sb.ToString();
        }

        /// <summary>
        /// 密码键盘签到
        /// </summary>
        /// <returns></returns>
        public ABCPayResult singIn()
        {
            String strOut = call("L|000000000000|01|123456|001|||");
            return stringToPayResult(strOut);
        }

        public List<string> print()
        {
            return new List<string>();
        }
    }
}
