﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace YunLuPos.Utils
{
    public class InIUtil
    {
        public static string ConfigPath = Environment.CurrentDirectory + "\\CONFIG.INI";
        public static string GenSection       = "GEN";          //基础配置
        public static string GenFullScreen = "G_FULL_SCREEN";          //全屏
        public static string GenShowTop = "G_SHOW_TOP";          //显示工具条
        public static string GenWidth = "G_WIDTH";          //窗体宽度
        public static string GenHeight = "G_HEIGHT";          //窗体高度
        public static string GenStyle = "G_STYLE";          //窗体高度
        public static string G_SHOW_SHORT = "G_SHOW_SHORT"; //默认显示快捷方式
        public static string G_ORDER_OWNER = "G_ORDER_OWNER"; //默认显示快捷方式
        public static string G_BOX_PWD = "G_BOX_PWD"; //开钱箱是否需要密码
        public static string G_PAY_SELECT_KEY = "G_PAY_SELECT_KEY"; //默认显示快捷方式

        public static string WeightSection = "WEIGHT";          //称重配置
        public static string W_ENABLE = "W_ENABLE";          //启用称重配置
        public static string W_PORT = "W_PORT";          //串口
        public static string W_RATE = "W_RATE";          //波特率
        public static string W_BIT = "W_BIT";          //数据位
        public static string W_STOPBIT = "W_STOPBIT";   //停止位
        public static string W_CHECKBIT = "W_CHECKBIT"; //校验位

        public static string PrintSection     = "PRINT";            //打印配置
        public static string PrintOption      = "PRINT_OPTION";     //打印类型
        public static string PrintComPort     = "COM_PORT";         //COM端口
        public static string PrintComRate     = "COM_RATE";         //COM波特率
        public static string PrintComBit      = "COM_BIT";          //COM数据位
        public static string PrintComStopBit  = "COM_STOPBIT";      //COM停止位
        public static string PrintComCheckBit = "COM_CHECKBIT";     //COM校验位
        public static string LptProt          = "LPT_PORT";         //并口端口
        public static string PrintFile        = "PRINT_FILE";       //打印文件名称

        public static string DisplaySection = "DISPLAY";            //客显
        public static string DisplayComPort = "D_COM_PORT";         //COM端口
        public static string DisplayComRate = "D_COM_RATE";         //COM波特率
        public static string DisplayComBit = "D_COM_BIT";          //COM数据位
        public static string DisplayComStopBit = "D_COM_STOPBIT";      //COM停止位
        public static string DisplayComCheckBit = "D_COM_CHECKBIT";     //COM校验位

        public static string BalanceSection = "BALANCE";            //称重码
        public static string BalanceStart = "B_START";         //COM端口
        public static string BalanceEnd = "B_END";         //COM波特率
        public static string Balance13 = "B_13";          //COM数据位
        public static string Balance18 = "B_18";      //COM停止位
        public static string BalancePriceMode = "BalancePriceMode";      //1,使用系统价  2，称台升高使用系统价  3,使用称台价（不检查称台变价）



        /// <summary>
        /// 写入基础配置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool WriteGenData(string key,string value)
        {
            return WriteInIData(GenSection, key, value, ConfigPath);
        }

        /// <summary>
        /// 写入配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="path"></param>
        /// <returns></returns>
       public static bool WriteInIData(string section, string key, string value)
        {
            return WriteInIData(section, key, value, ConfigPath);
        }

        /// <summary>
        /// 写入配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        static bool WriteInIData(string section, string key, string value, string path)
        {
            if (File.Exists(path))
            {
                return WritePrivateProfileString(section, key, value, path);
            }
            else
            {
                return false;
            }
        }




        /// <summary>
        /// 读取基础配置
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ReadGenData(string key)
        {
            return ReadInIData(GenSection, key, "", ConfigPath);
        }

        /// <summary>
        /// 根据配置文件读取配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="noText">默认值</param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ReadInIData(string section, string key)
        {
            return ReadInIData(section, key, "", ConfigPath);
        }

        public static string ReadInIData(string section, string key, string defaultValue)
        {
            return ReadInIData(section, key, defaultValue, ConfigPath);
        }

        /// <summary>
        /// 根据配置文件读取配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="noText">默认值</param>
        /// <param name="path"></param>
        /// <returns></returns>
        static string ReadInIData(string section,string key,string noText,string path)
        {
            if (File.Exists(path))
            {
                StringBuilder temp = new StringBuilder(1024);
                GetPrivateProfileString(section, key, noText, temp,1024, path);
                return temp.ToString();
            }else
            {
                return string.Empty;
            }
        }

        [DllImport("kernel32")]
        private static extern bool WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

    }
}
