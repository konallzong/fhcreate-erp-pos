﻿
namespace YunLuPos.SellView
{
    partial class SellMemberPay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCardNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lebName = new System.Windows.Forms.Label();
            this.lebPhone = new System.Windows.Forms.Label();
            this.lebAmount = new System.Windows.Forms.Label();
            this.payAmount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.flowNum = new System.Windows.Forms.Label();
            this.payWorker = new System.ComponentModel.BackgroundWorker();
            this.progress = new Sunny.UI.UIProgressIndicator();
            this.message = new Sunny.UI.UILabel();
            this.label7 = new System.Windows.Forms.Label();
            this.labCarNum = new System.Windows.Forms.Label();
            this.cardReadThread = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // txtCardNum
            // 
            this.txtCardNum.Location = new System.Drawing.Point(103, 116);
            this.txtCardNum.Name = "txtCardNum";
            this.txtCardNum.Size = new System.Drawing.Size(369, 29);
            this.txtCardNum.TabIndex = 2;
            this.txtCardNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCardNum_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 119);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "卡   号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "姓   名：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(278, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "手机号：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(278, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 21);
            this.label5.TabIndex = 6;
            this.label5.Text = "余   额：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "金   额：";
            // 
            // lebName
            // 
            this.lebName.AutoSize = true;
            this.lebName.Location = new System.Drawing.Point(100, 158);
            this.lebName.Name = "lebName";
            this.lebName.Size = new System.Drawing.Size(17, 21);
            this.lebName.TabIndex = 9;
            this.lebName.Text = "*";
            // 
            // lebPhone
            // 
            this.lebPhone.AutoSize = true;
            this.lebPhone.Location = new System.Drawing.Point(344, 158);
            this.lebPhone.Name = "lebPhone";
            this.lebPhone.Size = new System.Drawing.Size(17, 21);
            this.lebPhone.TabIndex = 10;
            this.lebPhone.Text = "*";
            // 
            // lebAmount
            // 
            this.lebAmount.AutoSize = true;
            this.lebAmount.Location = new System.Drawing.Point(344, 195);
            this.lebAmount.Name = "lebAmount";
            this.lebAmount.Size = new System.Drawing.Size(17, 21);
            this.lebAmount.TabIndex = 11;
            this.lebAmount.Text = "*";
            // 
            // payAmount
            // 
            this.payAmount.AutoSize = true;
            this.payAmount.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold);
            this.payAmount.Location = new System.Drawing.Point(99, 77);
            this.payAmount.Name = "payAmount";
            this.payAmount.Size = new System.Drawing.Size(26, 26);
            this.payAmount.TabIndex = 12;
            this.payAmount.Text = "{}";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 21);
            this.label6.TabIndex = 13;
            this.label6.Text = "流水号：";
            // 
            // flowNum
            // 
            this.flowNum.AutoSize = true;
            this.flowNum.Location = new System.Drawing.Point(99, 42);
            this.flowNum.Name = "flowNum";
            this.flowNum.Size = new System.Drawing.Size(20, 21);
            this.flowNum.TabIndex = 14;
            this.flowNum.Text = "{}";
            // 
            // payWorker
            // 
            this.payWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.payWorker_DoWork);
            this.payWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.payWorker_ProgressChanged);
            this.payWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.payWorker_RunWorkerCompleted);
            // 
            // progress
            // 
            this.progress.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.progress.Location = new System.Drawing.Point(23, 226);
            this.progress.MinimumSize = new System.Drawing.Size(1, 1);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(55, 55);
            this.progress.TabIndex = 16;
            this.progress.Text = "uiProgressIndicator1";
            // 
            // message
            // 
            this.message.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.message.Location = new System.Drawing.Point(103, 231);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(369, 46);
            this.message.TabIndex = 15;
            this.message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 195);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 21);
            this.label7.TabIndex = 17;
            this.label7.Text = "卡   号：";
            // 
            // labCarNum
            // 
            this.labCarNum.AutoSize = true;
            this.labCarNum.Location = new System.Drawing.Point(100, 195);
            this.labCarNum.Name = "labCarNum";
            this.labCarNum.Size = new System.Drawing.Size(17, 21);
            this.labCarNum.TabIndex = 18;
            this.labCarNum.Text = "*";
            // 
            // cardReadThread
            // 
            this.cardReadThread.WorkerSupportsCancellation = true;
            this.cardReadThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.cardReadThread_DoWork);
            this.cardReadThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.cardReadThread_RunWorkerCompleted);
            // 
            // SellMemberPay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 294);
            this.Controls.Add(this.labCarNum);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.message);
            this.Controls.Add(this.flowNum);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.payAmount);
            this.Controls.Add(this.lebAmount);
            this.Controls.Add(this.lebPhone);
            this.Controls.Add(this.lebName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCardNum);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SellMemberPay";
            this.Text = "储值卡支付";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SellMemberPay_FormClosing);
            this.Load += new System.EventHandler(this.SellMemberPay_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCardNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lebName;
        private System.Windows.Forms.Label lebPhone;
        private System.Windows.Forms.Label lebAmount;
        private System.Windows.Forms.Label payAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label flowNum;
        private System.ComponentModel.BackgroundWorker payWorker;
        private Sunny.UI.UIProgressIndicator progress;
        private Sunny.UI.UILabel message;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labCarNum;
        private System.ComponentModel.BackgroundWorker cardReadThread;
    }
}