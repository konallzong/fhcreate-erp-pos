﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Utils;
using YunLuPos.Worker;

namespace YunLuPos.SellView
{
    public partial class SellLoginFrom : UIForm
    {
        private String[] styles = new String[] { "1","2","3","4","5", "6", "7","8","101","102","103"};
        LoadServerDataWorker serverDataWorker = new LoadServerDataWorker();
        private CashierService cashierService = new CashierService();
        public SellLoginFrom()
        {
            InitializeComponent();
        }

        private void SellLoginFrom_Load(object sender, EventArgs e)
        {
            
            try
            {
                int w = Int32.Parse(StaticInfoHoder.Width);
                int h = Int32.Parse(StaticInfoHoder.Height);
                this.Width = w;
                this.Height = h;
                this.ShowTitle = (StaticInfoHoder.ShowTop == "1");
                if(StaticInfoHoder.FullScreen == "1")
                {
                    this.WindowState = FormWindowState.Maximized;
                }
                else
                {
                    this.WindowState = FormWindowState.Normal;
                }

            }
            catch
            {

            }
            this.codeInput.Enabled = false;
            this.pwdInput.Enabled = false;
            downWorker.RunWorkerAsync();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (this.pwdInput.Text != null && !"".Equals(this.pwdInput.Text))
            {
                this.codeInput.Focus();
                bool b = doLogin();
                this.codeInput.Text = "";
                this.pwdInput.Text = "";
                this.codeInput.Enabled = true;
                this.codeInput.Focus();
                if (b)
                {
                    SellMainForm main = new SellMainForm(this);
                    main.Show();
                    this.Hide();
                }
            }
            else
            {
                this.codeInput.Focus();
            }
        }

        private void pwdInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (this.pwdInput.Text != null && !"".Equals(this.pwdInput.Text))
                {
                    this.codeInput.Focus();
                    bool b = doLogin();
                    this.codeInput.Text = "";
                    this.pwdInput.Text = "";
                    this.codeInput.Enabled = true;
                    this.codeInput.Focus();
                    if (b)
                    {
                        SellMainForm main = new SellMainForm(this);
                        main.Show();
                        this.Hide();
                    }
                }
                else
                {
                    this.codeInput.Focus();
                }

                e.Handled = true;
            }
        }

        private void codeInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 && this.codeInput.Text != null && !"".Equals(this.codeInput.Text))
            {
                pwdInput.Focus();
            }
        }

        Boolean doLogin()
        {
            Cashier cashier = cashierService.doLogin(this.codeInput.Text.Trim(), this.pwdInput.Text.Trim());
            if (cashier != null)
            {
                StaticInfoHoder.commInfo.cashierCode = cashier.cashierCode;
                StaticInfoHoder.commInfo.cashierName = cashier.cashierName;
            }else
            {
                this.ShowInfoDialog("工号或密码错误！", this.Style);
            }
            return cashier != null;
        }

        private void downWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            serverDataWorker.syncWithReport(downWorker);
        }

        private void downWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.taskMessage.Text = e.UserState.ToString();
            Console.WriteLine(e.ProgressPercentage);
            syncProcess.Value = e.ProgressPercentage;
        }

        private void downWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("执行完成");
            this.codeInput.Enabled = true;
            this.pwdInput.Enabled = true;
            this.codeInput.Focus();
        }


        public void setLockInfo(String code)
        {
            this.codeInput.Text = code;
            this.codeInput.Enabled = false;
            this.pwdInput.Focus();
        }

        private void SellLoginFrom_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String fileStr = File.ReadAllText("c:/LINCENSE");
            String jsonString = DESUtils.decrypt(fileStr);
            Console.WriteLine(jsonString);
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            Console.WriteLine("============");

            SendKeys.Send(((UIButton)sender).Text);
        }

        bool isCode = true;
        private void codeInput_Leave(object sender, EventArgs e)
        {
            isCode = true;
        }

        private void pwdInput_Leave(object sender, EventArgs e)
        {
            isCode = false;
        }

        private void SellLoginFrom_FormClosing(object sender, FormClosingEventArgs e)
        {
            ExitForm ex = new ExitForm();
            if(!( ex.ShowDialog() == DialogResult.OK))
            {
                e.Cancel = true;
            }
        }

        private void SellLoginFrom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                for(int i = 0; i < styles.Length; i++)
                {
                    if (styles[i].Equals(StaticInfoHoder.Style))
                    {
                        if(i < styles.Length-1)
                        {
                            StaticInfoHoder.Style = styles[i + 1];
                            break;
                        }else
                        {
                            StaticInfoHoder.Style = styles[0];
                            break;
                        }
                    }
                }
                try
                {
                    UIStyleManager um = new UIStyleManager();
                    um.Style = (UIStyle)Int32.Parse(StaticInfoHoder.Style);
                }
                catch { }
                InIUtil.WriteInIData(InIUtil.GenSection, InIUtil.GenStyle, StaticInfoHoder.Style);

            }
        }


    }
}
