﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using HisPay.Payment;

namespace YunLuPos.View
{
    public partial class HisPayForm : BaseDialogForm
    {
        private Double amount = 0;
        private String outTradeNo = "";
        public Dictionary<String, Object> result = null;
        public HisPayForm(String outTradeNo,Double amount)
        {
            InitializeComponent();
            this.amount = amount;
            this.outTradeNo = outTradeNo;
            this.amountLabel.Text = amount.ToString("F2");
            this.orderCode.Text = outTradeNo;
        }

        private void SacnPayCodeForm_Load(object sender, EventArgs e)
        {
            this.ActiveControl = this.authCode;
        }

        private void authCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            const char Enter = (char)13;
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != Delete
                        && e.KeyChar != Enter)
            {
                e.Handled = true;
            }
            else if (e.KeyChar == Enter)
            {
                this.message.Text = "*";
                String code = authCode.Text;
                this.authCode.Text = "";
                //微信支付宝扫码付
                Dictionary<String, Object> objectMap = new Dictionary<string, object>();
                objectMap.Add("SHOP_CODE", StaticInfoHoder.commInfo.shopCode);
                objectMap.Add("DEVICE_INFO", StaticInfoHoder.commInfo.posCode);
                objectMap.Add("OPERATOR_ID", StaticInfoHoder.commInfo.cashierCode);
                objectMap.Add("BUSINESS_TYPE", "1");
                objectMap.Add("OUT_TRADE_NO", outTradeNo);
                objectMap.Add("TOTAL_FEE", amount * 100);
                objectMap.Add("AUTH_CODE", code);
                Dictionary<String, object> result = PayClient.send(objectMap, null);
                if (result != null && result.ContainsKey(C.RESULT_CODE) && C.SUCCESS.Equals(result[C.RESULT_CODE]))
                {
                    //支付成功
                    this.DialogResult = DialogResult.OK;
                    this.result = result;
                    this.Close();
                }
                else
                {
                    //支付失败
                    String failMsg = String.Format("错误码：{0}\r\n信  息:{1}", result[C.FAIL_CODE], result[C.RESULT_MSG]);
                    if (result.ContainsKey("PAY_WAY"))
                    {
                        failMsg = "通道码:" + result["PAY_WAY"] + "\r\n" + failMsg;
                    }
                    this.message.Text = failMsg;
                }
            }
        }
    }
}
