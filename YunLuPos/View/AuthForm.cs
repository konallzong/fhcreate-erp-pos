﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace YunLuPos.View
{
    public partial class AuthForm : BaseDialogForm
    {
        CashierService cashierService = new CashierService();
        String commandKey = null;
        public Cashier authCashier = null;
        public AuthForm(String commandKey)
        {
            InitializeComponent();
            this.commandKey = commandKey;
        }

        private void codeInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 && this.codeInput.Text != null && !"".Equals(this.codeInput.Text))
            {
                pwdInput.Focus();
            }
        }

        private void pwdInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 )
            {
                doAuth();
                e.Handled = true;
            }
        }

        private void baseButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void baseButton1_Click(object sender, EventArgs e)
        {
            doAuth();
        }

        void doAuth()
        {
            if(this.pwdInput.Text != null && !"".Equals(this.pwdInput.Text))
            {
                this.codeInput.Focus();
                Cashier cashier = cashierService.doAuth(this.codeInput.Text, this.pwdInput.Text, commandKey, true);
                this.codeInput.Text = "";
                this.pwdInput.Text = "";
                if (cashier != null)
                {
                    this.authCashier = cashier;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    this.messageLabel.Text = "账号密码错误或无权限";
                }
            }
        }
    }
}
