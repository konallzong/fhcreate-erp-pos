﻿namespace YunLuPos.View
{
    partial class SacnPayCodeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseLabel1 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel2 = new YunLuPos.Com.BaseLabel(this.components);
            this.amountLabel = new YunLuPos.Com.BaseLabel(this.components);
            this.authCode = new YunLuPos.Com.BaseTextInput(this.components);
            this.baseLabel3 = new YunLuPos.Com.BaseLabel(this.components);
            this.message = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel4 = new YunLuPos.Com.BaseLabel(this.components);
            this.orderCode = new YunLuPos.Com.BaseLabel(this.components);
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Size = new System.Drawing.Size(331, 41);
            this.label1.Text = "请扫付款码";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Size = new System.Drawing.Size(331, 234);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.orderCode);
            this.panel3.Controls.Add(this.baseLabel4);
            this.panel3.Controls.Add(this.message);
            this.panel3.Controls.Add(this.baseLabel3);
            this.panel3.Controls.Add(this.authCode);
            this.panel3.Controls.Add(this.amountLabel);
            this.panel3.Controls.Add(this.baseLabel2);
            this.panel3.Controls.Add(this.baseLabel1);
            this.panel3.Size = new System.Drawing.Size(331, 193);
            // 
            // baseLabel1
            // 
            this.baseLabel1.AutoSize = true;
            this.baseLabel1.ForeColor = System.Drawing.Color.White;
            this.baseLabel1.Location = new System.Drawing.Point(12, 60);
            this.baseLabel1.Name = "baseLabel1";
            this.baseLabel1.Size = new System.Drawing.Size(47, 12);
            this.baseLabel1.TabIndex = 0;
            this.baseLabel1.Text = "金  额:";
            // 
            // baseLabel2
            // 
            this.baseLabel2.AutoSize = true;
            this.baseLabel2.ForeColor = System.Drawing.Color.White;
            this.baseLabel2.Location = new System.Drawing.Point(12, 97);
            this.baseLabel2.Name = "baseLabel2";
            this.baseLabel2.Size = new System.Drawing.Size(47, 12);
            this.baseLabel2.TabIndex = 1;
            this.baseLabel2.Text = "付款码:";
            // 
            // amountLabel
            // 
            this.amountLabel.AutoSize = true;
            this.amountLabel.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountLabel.ForeColor = System.Drawing.Color.Gold;
            this.amountLabel.Location = new System.Drawing.Point(65, 54);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(58, 24);
            this.amountLabel.TabIndex = 2;
            this.amountLabel.Text = "0.00";
            // 
            // authCode
            // 
            this.authCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.authCode.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authCode.Location = new System.Drawing.Point(67, 90);
            this.authCode.Name = "authCode";
            this.authCode.Size = new System.Drawing.Size(244, 26);
            this.authCode.TabIndex = 3;
            this.authCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.authCode_KeyPress);
            // 
            // baseLabel3
            // 
            this.baseLabel3.AutoSize = true;
            this.baseLabel3.ForeColor = System.Drawing.Color.White;
            this.baseLabel3.Location = new System.Drawing.Point(12, 131);
            this.baseLabel3.Name = "baseLabel3";
            this.baseLabel3.Size = new System.Drawing.Size(47, 12);
            this.baseLabel3.TabIndex = 4;
            this.baseLabel3.Text = "消  息:";
            // 
            // message
            // 
            this.message.ForeColor = System.Drawing.Color.White;
            this.message.Location = new System.Drawing.Point(67, 131);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(244, 54);
            this.message.TabIndex = 5;
            this.message.Text = "*";
            // 
            // baseLabel4
            // 
            this.baseLabel4.AutoSize = true;
            this.baseLabel4.ForeColor = System.Drawing.Color.White;
            this.baseLabel4.Location = new System.Drawing.Point(12, 26);
            this.baseLabel4.Name = "baseLabel4";
            this.baseLabel4.Size = new System.Drawing.Size(47, 12);
            this.baseLabel4.TabIndex = 6;
            this.baseLabel4.Text = "流水号:";
            // 
            // orderCode
            // 
            this.orderCode.AutoSize = true;
            this.orderCode.ForeColor = System.Drawing.Color.White;
            this.orderCode.Location = new System.Drawing.Point(67, 26);
            this.orderCode.Name = "orderCode";
            this.orderCode.Size = new System.Drawing.Size(47, 12);
            this.orderCode.TabIndex = 7;
            this.orderCode.Text = "0000000";
            // 
            // SacnPayCodeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 240);
            this.Name = "SacnPayCodeForm";
            this.Text = "SacnPayCodeForm";
            this.Load += new System.EventHandler(this.SacnPayCodeForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Com.BaseLabel baseLabel1;
        private Com.BaseLabel baseLabel2;
        private Com.BaseLabel amountLabel;
        private Com.BaseTextInput authCode;
        private Com.BaseLabel baseLabel3;
        private Com.BaseLabel message;
        private Com.BaseLabel baseLabel4;
        private Com.BaseLabel orderCode;
    }
}