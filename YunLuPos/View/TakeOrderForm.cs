﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace YunLuPos.View
{
    public partial class TakeOrderForm : BaseDialogForm
    {
        public SaleOrder selectedOrder = null;
        private SaleOrderService orderService = new SaleOrderService();
        public TakeOrderForm()
        {
            InitializeComponent();
        }

        private void TakeOrderForm_Load(object sender, EventArgs e)
        {
            this.orderGrid.DataSource = orderService.listHoderOrder();

        }

        /// <summary>
        /// 重写键盘响应事件。拦截回车解决表格回车下一行问题
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter && this.orderGrid.Focused)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void orderGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (orderGrid.CurrentRow != null)
            {
                SaleOrder order = orderGrid.CurrentRow.DataBoundItem as SaleOrder;
                if (!order.orderCode.Equals(this.selectedOrder))
                {
                    this.selectedOrder = orderService.getFullOrder(order.orderCode);
                    this.goodsGrid.DataSource = selectedOrder.goods;
                }
            }
        }

        private void TakeOrderForm_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Up)
            {
                orderGrid.moveUp();
            }else if(e.KeyCode == Keys.Down)
            {
                orderGrid.moveDown();
            }else if (e.KeyCode == Keys.Enter)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void baseButton1_Click(object sender, EventArgs e)
        { 
            this.DialogResult = DialogResult.OK;
            this.Close();
         
        }

        private void baseButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
