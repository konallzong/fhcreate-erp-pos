﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Net;
using YunLuPos.Payment;
using YunLuPos.Printer;
using YunLuPos.SellView;
using YunLuPos.View;
using YunLuPos.Worker;

namespace YunLuPos.Command
{
    class PaymentCommand : CommandInterface
    {
        SyncOrderWorker woker = new SyncOrderWorker();
        private SaleOrderService orderService = new SaleOrderService();
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        private PayTypeService payTypeService = new PayTypeService();
        public void exec(Form handel, Cashier auther)
        {
            if(DynamicInfoHoder.currentOrder.goods == null || DynamicInfoHoder.currentOrder.goods.Count <= 0)
            {
                return;
            }

            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                PayForm pf = new PayForm(DynamicInfoHoder.currentOrder);
                if(pf.ShowDialog() == DialogResult.OK)
                {
                    PaymentState state = pf.state;
                    if(state != null && state.less == 0)
                    {
                        bool b = orderService.done(DynamicInfoHoder.currentOrder,state);
                        if (b)
                        {
                            doneAfter();
                            //创建新订单
                            mf.createNewOrder();
                        }
                    }
                }
            }


            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                SellPayForm payForm = new SellPayForm(DynamicInfoHoder.currentOrder);
                payForm.Style = smf.Style;
                if(payForm.ShowDialog() == DialogResult.OK)
                {
                    PaymentState state = payForm.state;
                    if (state != null && state.less == 0)
                    {
                        bool b = orderService.done(DynamicInfoHoder.currentOrder, state);
                        if (b)
                        {
                            doneAfter();
                            //创建新订单
                            smf.createNewOrder();
                        }
                    }
                }
            }
        }

        public void doneAfter()
        {
            //调用后台传单
            woker.asynStart(true, 0);
            //打印订单
            List<string> list = orderService.PrintList(DynamicInfoHoder.currentOrder.orderCode);
            PrintBase pb = PrinterManager.getPrinter();

            if (pb != null)
            {
                pb.initPrinter();
                pb.open();
                pb.writeLine(StaticInfoHoder.commInfo.cliqueName + "[" + StaticInfoHoder.commInfo.shopName + "]");
                pb.printList(list);
                //打印签购单
                List<String> keys = accountService.listMasterKeys(DynamicInfoHoder.currentOrder.orderCode);
                if (keys != null)
                {
                    foreach (String key in keys)
                    {
                        List<String> pList = PaymentManager.print(key);
                        if (pList != null && pList.Count > 0)
                        {
                            pb.printList(pList);
                        }
                    }
                }
                //根据支付方式判断是否打开钱箱
                if (keys != null)
                {
                    foreach (String key in keys)
                    {
                        PayType type = payTypeService.getTypeByKey(key);
                        if (type != null && ("1".Equals(type.openBox) || type.openBox == null))
                        {
                            pb.openMoneyBox();
                            break;
                        }
                    }
                }

                pb.NewRow();
                pb.NewRow();
                pb.NewRow();
                pb.NewRow();
                pb.NewRow();
                pb.NewRow();
                pb.Dispose();
            }
            
        }

        public string getKey()
        {
            return Names.Payment.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
