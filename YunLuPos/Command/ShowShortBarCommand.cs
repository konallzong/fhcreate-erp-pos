﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    /**
     * 获取光标命令
     * */
    public class ShowShortBarCommand : CommandInterface
    {
        public void exec(Form handel, Cashier cashier)
        {
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                mf.ActiveControl = mf.goodsInfoPanel1.codeInput;
            }

            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                if (!smf.shortPanel.Visible)
                {
                    smf.showShortBar();
                }
                else
                {
                    smf.hideShortBar();
                }
            }
        }

        public string getKey()
        {
            return Names.ShowShortBar.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
