﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;
using YunLuPos.View;

namespace YunLuPos.Command
{
    class TakeOrderCommand : CommandInterface
    {
        private SaleOrderService orderService = new SaleOrderService();
        public void exec(Form handel, Cashier auther)
        {
            if (DynamicInfoHoder.currentOrder.goods != null && DynamicInfoHoder.currentOrder.goods.Count > 0)
            {
                return;
            }
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                TakeOrderForm takeForm = new TakeOrderForm();
                if(takeForm.ShowDialog() == DialogResult.OK)
                {
                    if(takeForm.selectedOrder != null)
                    {
                        Boolean b = orderService.hoderOrder(takeForm.selectedOrder, false);//false 解挂
                        if (b)
                        {
                            DynamicInfoHoder.currentOrder = takeForm.selectedOrder;
                            mf.displayCurrentOrder(true,true);
                        }
                    }
                }
            }

            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                SellTackOrderForm takeForm = new SellTackOrderForm();
                takeForm.Style = smf.Style;
                if (takeForm.ShowDialog() == DialogResult.OK)
                {
                    if (takeForm.selectedOrder != null)
                    {
                        Boolean b = orderService.hoderOrder(takeForm.selectedOrder, false);//false 解挂
                        if (b)
                        {
                            DynamicInfoHoder.currentOrder = takeForm.selectedOrder;
                            smf.displayCurrentOrder(true, true);
                        }
                    }
                }
            }

        }

        public string getKey()
        {
            return Names.TakeOrder.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
