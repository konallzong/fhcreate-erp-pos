﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;

namespace YunLuPos.Command
{
    public interface CommandInterface
    {
        /**
         * 是否将命令纳入权限管理
         * */
        Boolean needAuth();
        /**
         * 命令名称
         * */
        String getKey();

        /**
         * 执行命令
         * */
        void exec(Form handel,Cashier auther);
    }
}
