﻿namespace YunLuPos.UIComponent
{
    partial class UIPreOrderInfoPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.changeAmount = new Sunny.UI.UILabel();
            this.goodsCount = new Sunny.UI.UILabel();
            this.payAmount = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.uiLabel4);
            this.uiPanel1.Controls.Add(this.changeAmount);
            this.uiPanel1.Controls.Add(this.goodsCount);
            this.uiPanel1.Controls.Add(this.payAmount);
            this.uiPanel1.Controls.Add(this.uiLabel3);
            this.uiPanel1.Controls.Add(this.uiLabel2);
            this.uiPanel1.Controls.Add(this.uiLabel1);
            this.uiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(203, 108);
            this.uiPanel1.TabIndex = 0;
            this.uiPanel1.Text = null;
            // 
            // changeAmount
            // 
            this.changeAmount.BackColor = System.Drawing.Color.Transparent;
            this.changeAmount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.changeAmount.Location = new System.Drawing.Point(54, 78);
            this.changeAmount.Name = "changeAmount";
            this.changeAmount.Size = new System.Drawing.Size(116, 23);
            this.changeAmount.TabIndex = 5;
            this.changeAmount.Text = "0.00";
            this.changeAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // goodsCount
            // 
            this.goodsCount.BackColor = System.Drawing.Color.Transparent;
            this.goodsCount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.goodsCount.Location = new System.Drawing.Point(53, 52);
            this.goodsCount.Name = "goodsCount";
            this.goodsCount.Size = new System.Drawing.Size(116, 23);
            this.goodsCount.TabIndex = 4;
            this.goodsCount.Text = "0.00";
            this.goodsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // payAmount
            // 
            this.payAmount.BackColor = System.Drawing.Color.Transparent;
            this.payAmount.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.payAmount.Location = new System.Drawing.Point(54, 27);
            this.payAmount.Name = "payAmount";
            this.payAmount.Size = new System.Drawing.Size(116, 23);
            this.payAmount.TabIndex = 3;
            this.payAmount.Text = "0.00";
            this.payAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(10, 78);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(48, 23);
            this.uiLabel3.TabIndex = 2;
            this.uiLabel3.Text = "找零:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(9, 52);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(49, 23);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "数量:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(9, 27);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(49, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "合计:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel4
            // 
            this.uiLabel4.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel4.Location = new System.Drawing.Point(10, 4);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(101, 23);
            this.uiLabel4.TabIndex = 6;
            this.uiLabel4.Text = "上单信息";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UIPreOrderInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiPanel1);
            this.Name = "UIPreOrderInfoPanel";
            this.Size = new System.Drawing.Size(203, 108);
            this.uiPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel changeAmount;
        private Sunny.UI.UILabel goodsCount;
        private Sunny.UI.UILabel payAmount;
        public Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UILabel uiLabel4;
    }
}
