﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;
using YunLuPos.Entity.Constant;
using YunLuPos.Com;

namespace YunLuPos.UIComponent
{
    public partial class UITopInfoPanel : UserControl
    {
        public UITopInfoPanel()
        {
            InitializeComponent();
        }

        private void UITopInfoPanel_Load(object sender, EventArgs e)
        {
            
        }

        public void setCommInfo(CommInfo comminfo)
        {
            if (comminfo == null)
            {
                this.shopName.Text = "";
                this.cashierCode.Text = "";
                this.cashierName.Text = "";
                this.posCode.Text = "";
            }
            else
            {
                this.shopName.Text = comminfo.shopName + "["+comminfo.cliqueName+"]";
                this.cashierCode.Text = comminfo.cashierCode;
                this.cashierName.Text = comminfo.cashierName;
                this.posCode.Text = comminfo.posCode;
            }
        }

        public void setOrderInfo(SaleOrder order)
        {
            this.orderNo.Text = order.orderCode;
            if (SaleOrderType.SALE.ToString().Equals(order.orderType))
            {
                this.orderType.ForeColor = Color.Blue;
                this.orderType.Text = "销售";

            }
            else if (SaleOrderType.REJECT.ToString().Equals(order.orderType))
            {
                this.orderType.ForeColor = Colors.errorColor;
                this.orderType.Text = "退货";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            date.Text = dt.ToString("yyyy-MM-dd");
            time.Text = dt.ToString("HH:mm:ss");
        }

        /**
       * 刷新网络状况
       * */
        public void setNetStats(Boolean isAlive)
        {
            if (isAlive)
            {
                this.pictureBox2.BackgroundImage = global::YunLuPos.Properties.Resources.net_alive;
                this.netDes.Text = "联机";
            }
            else
            {
                this.pictureBox2.BackgroundImage = global::YunLuPos.Properties.Resources.net_down;
                this.netDes.Text = "离线";
            }
        }
    }
}
