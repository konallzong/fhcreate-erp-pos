﻿namespace YunLuPos.UIComponent
{
    partial class UIGoodsInfoPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.uiPanel1 = new Sunny.UI.UIPanel();
            this.unit = new Sunny.UI.UILabel();
            this.specs = new Sunny.UI.UILabel();
            this.goodsName = new Sunny.UI.UILabel();
            this.price = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.codeInput = new Sunny.UI.UITextBox();
            this.uiPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPanel1
            // 
            this.uiPanel1.Controls.Add(this.unit);
            this.uiPanel1.Controls.Add(this.specs);
            this.uiPanel1.Controls.Add(this.goodsName);
            this.uiPanel1.Controls.Add(this.price);
            this.uiPanel1.Controls.Add(this.uiLabel3);
            this.uiPanel1.Controls.Add(this.uiLabel2);
            this.uiPanel1.Controls.Add(this.uiLabel1);
            this.uiPanel1.Controls.Add(this.codeInput);
            this.uiPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiPanel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanel1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(281, 108);
            this.uiPanel1.TabIndex = 0;
            this.uiPanel1.Text = null;
            // 
            // unit
            // 
            this.unit.BackColor = System.Drawing.Color.Transparent;
            this.unit.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.unit.Location = new System.Drawing.Point(149, 70);
            this.unit.Name = "unit";
            this.unit.Size = new System.Drawing.Size(49, 23);
            this.unit.TabIndex = 7;
            this.unit.Text = "无";
            this.unit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // specs
            // 
            this.specs.BackColor = System.Drawing.Color.Transparent;
            this.specs.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.specs.Location = new System.Drawing.Point(51, 70);
            this.specs.Name = "specs";
            this.specs.Size = new System.Drawing.Size(49, 23);
            this.specs.TabIndex = 6;
            this.specs.Text = "无";
            this.specs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // goodsName
            // 
            this.goodsName.BackColor = System.Drawing.Color.Transparent;
            this.goodsName.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.goodsName.Location = new System.Drawing.Point(49, 42);
            this.goodsName.Name = "goodsName";
            this.goodsName.Size = new System.Drawing.Size(229, 23);
            this.goodsName.TabIndex = 5;
            this.goodsName.Text = "无";
            this.goodsName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // price
            // 
            this.price.BackColor = System.Drawing.Color.Transparent;
            this.price.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.price.Location = new System.Drawing.Point(179, 11);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(99, 23);
            this.price.TabIndex = 4;
            this.price.Text = "0.00";
            this.price.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel3
            // 
            this.uiLabel3.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel3.Location = new System.Drawing.Point(106, 70);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(49, 23);
            this.uiLabel3.TabIndex = 3;
            this.uiLabel3.Text = "单位:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            this.uiLabel2.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel2.Location = new System.Drawing.Point(6, 70);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(49, 23);
            this.uiLabel2.TabIndex = 2;
            this.uiLabel2.Text = "规格:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uiLabel1
            // 
            this.uiLabel1.BackColor = System.Drawing.Color.Transparent;
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(6, 41);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(49, 23);
            this.uiLabel1.TabIndex = 1;
            this.uiLabel1.Text = "品名:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // codeInput
            // 
            this.codeInput.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.codeInput.FillColor = System.Drawing.Color.White;
            this.codeInput.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.codeInput.Location = new System.Drawing.Point(6, 8);
            this.codeInput.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.codeInput.Maximum = 2147483647D;
            this.codeInput.Minimum = -2147483648D;
            this.codeInput.MinimumSize = new System.Drawing.Size(1, 1);
            this.codeInput.Name = "codeInput";
            this.codeInput.Padding = new System.Windows.Forms.Padding(5);
            this.codeInput.Size = new System.Drawing.Size(166, 29);
            this.codeInput.TabIndex = 0;
            this.codeInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.codeInput_KeyPress);
            // 
            // UIGoodsInfoPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiPanel1);
            this.Name = "UIGoodsInfoPanel";
            this.Size = new System.Drawing.Size(281, 108);
            this.uiPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UILabel price;
        public Sunny.UI.UITextBox codeInput;
        private Sunny.UI.UILabel goodsName;
        private Sunny.UI.UILabel specs;
        private Sunny.UI.UILabel unit;
        public Sunny.UI.UIPanel uiPanel1;
    }
}
