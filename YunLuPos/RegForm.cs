﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Utils;
using YunLuPos.Entity;
using YunLuPos.Net;
using YunLuPos.Net.DTO;
using YunLuPos.Net.Http;

namespace YunLuPos
{
    public partial class RegForm : Form
    {
        CommonNetService commNet = NetServiceManager.getCommonNetService();
        PosInfo info = null;
        public RegForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(this.serverUrl.Text == null || "".Equals(this.serverUrl.Text.Trim()))
            {
                MessageBox.Show("请输入服务器地址");
                return;
            }
            if (this.regCode.Text == null || "".Equals(this.regCode.Text.Trim()))
            {
                MessageBox.Show("请输入初始化码");
                return;
            }
            HttpBase.ServerUrl = this.serverUrl.Text;
            RegCode dto = new RegCode();
            dto.code = this.regCode.Text;
            try
            {
                info = commNet.posInfo(dto);
                if (info != null)
                {
                    this.cliqueCode.Text = info.cliqueCode;
                    this.cliqueName.Text = info.cliqueName;
                    this.branchCode.Text = info.branchCode;
                    this.branchName.Text = info.branchName;
                    this.posCode.Text = info.posCode;
                    this.posName.Text = info.posName;
                    this.serverUrl.Enabled = false;
                    this.regCode.Enabled = false;
                }else
                {
                    MessageBox.Show("无效注册码");
                }
            }
            catch (NetException ne)
            {
                MessageBox.Show("服务器请求失败，请检查服务器地址！");
            }
            catch (BizError be)
            {
                MessageBox.Show(be.getMsg());
            }
            
        }

        private void RegForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(info == null)
            {
                MessageBox.Show("请加载初始化信息");
                return;
            }
            try
            {
                info.deviceCode = DriverUtil.getDriverNum();
                PosInfo pi = commNet.regPos(info);
                if (pi != null)
                {
                    RegInfo ri = new RegInfo();
                    ri.serverAddr = this.serverUrl.Text.Trim();
                    ri.regCode = pi.regCode;
                    ri.deviceNum = info.deviceCode;
                    ri.clientKey = pi.appKey;
                    ri.cliqueCode = pi.cliqueCode;
                    ri.cliqueName = info.cliqueName;
                    ri.branchCode = pi.branchCode;
                    ri.branchName = info.branchName;
                    ri.posCode = pi.posCode;
                    ri.posName = pi.posName;
                    ri.regTime = pi.createDate;
                    ri.expTime = pi.expDate;
                    bool b = Register.register(ri);
                    if (b)
                    {
                        MessageBox.Show("注册成功！");
                    }else
                    {
                        MessageBox.Show("写入授权文件失败！");
                    }
                }
                else
                {
                    MessageBox.Show("注册失败！");
                }
            }
            catch (NetException ne)
            {
                MessageBox.Show("服务器请求失败，请检查服务器地址！");
            }
            catch (BizError be)
            {
                MessageBox.Show(be.getMsg());
            }
        }
    }
}
