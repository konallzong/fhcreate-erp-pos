﻿using log4net;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using YunLuPos.DB.Utils;
using YunLuPos.Net.Http;
using YunLuPos.SellView;
using YunLuPos.TouchView;
using YunLuPos.Utils;

namespace YunLuPos
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            string strProcessName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            ////获取版本号 
            //CommonData.VersionNumber = Application.ProductVersion; 
            //检查进程是否已经启动，已经启动则显示报错信息退出程序。 
            if (System.Diagnostics.Process.GetProcessesByName(strProcessName).Length > 1)
            {

                MessageBox.Show("收银台正在运行，不可重复启动！", "消息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
                return;
            }
            //启用日志
            log4net.Config.XmlConfigurator.Configure();

            //处理UI线程异常  
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            //处理非UI线程异常  
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            RegInfo regInfo = getRegInfo();
            if(regInfo == null)
            {
                Application.Run(new RegForm());
            }
            else
            {
                //读取配置文件
                StaticInfoHoder.FullScreen =  InIUtil.ReadInIData(InIUtil.GenSection, InIUtil.GenFullScreen, "0");
                StaticInfoHoder.ShowTop = InIUtil.ReadInIData(InIUtil.GenSection, InIUtil.GenShowTop, "1");
                StaticInfoHoder.Width = InIUtil.ReadInIData(InIUtil.GenSection, InIUtil.GenWidth, "1000");
                StaticInfoHoder.Height = InIUtil.ReadInIData(InIUtil.GenSection, InIUtil.GenHeight, "700");
                StaticInfoHoder.Style = InIUtil.ReadInIData(InIUtil.GenSection, InIUtil.GenStyle, "1");
                StaticInfoHoder.PAY_SELECT_KEY = InIUtil.ReadInIData(InIUtil.GenSection, InIUtil.G_PAY_SELECT_KEY, "F2");
                try
                {
                    
                    UIStyleManager um = new UIStyleManager();
                    um.Style = (UIStyle)Int32.Parse(StaticInfoHoder.Style); 
                }
                catch { }
                

                HttpBase.ServerUrl = regInfo.serverAddr;
                StaticInfoHoder.commInfo.cliqueCode = regInfo.cliqueCode;
                StaticInfoHoder.commInfo.cliqueName = regInfo.cliqueName;
                StaticInfoHoder.commInfo.shopCode = regInfo.branchCode;
                StaticInfoHoder.commInfo.shopName = regInfo.branchName;
                StaticInfoHoder.commInfo.posCode = regInfo.posCode;
                StaticInfoHoder.commInfo.posName = regInfo.posName;
               
                Application.Run(new SellLoginFrom());
            }

        }

        /// <summary>
        /// 获取基础注册信息
        /// 获取失败提示重新注册
        /// </summary>
        /// <returns></returns>
        static RegInfo getRegInfo()
        {
            String mCode = DriverUtil.getDriverNum();
            return Register.check(mCode);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            if(e.Exception is NetException)
            {
                NetException ne = e.Exception as NetException;
                if(ne != null)
                {
                    Console.WriteLine(ne.getMsg());
                }
            }else
            {
                UIMessageBox.ShowError("UI线程错误，请联系管理员，或查看日志！");
                ILog logger = LogManager.GetLogger(typeof(Program));
                logger.Error("UI线程异常");
                logger.Error(e.Exception);
                Console.WriteLine(e.Exception);
            }

        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ILog logger = LogManager.GetLogger(typeof(Program));
            logger.Error(e.ExceptionObject);
            logger.Error(e);
            logger.Error("非UI线程异常");
            UIMessageBox.ShowError("系统线程错误，请联系管理员，或查看日志！");
            Console.WriteLine(e.ToString());
        }
    }
}
